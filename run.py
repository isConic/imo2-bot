import discord
import asyncio
import yaml

client = discord.Client()

configs = yaml.load(open("bot-config.yaml"))
token   = configs["bot_token"]

client.run(token)
